import math

number = int(input("Enter a number:"))

for current in range(3, int(math.sqrt(number) + 1), 2):
	if number % current != 0:
		print(current, " is prime...")