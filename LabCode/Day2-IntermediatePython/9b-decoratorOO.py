# Decorator on a method
def p_decorate(func):
   def func_wrapper(*args, **kwargs):
       return "<p>{0}</p>".format(func(*args, **kwargs))
   return func_wrapper

class Person(object):
    def __init__(self):
        self.first_name = "C"
        self.last_name = "B"

    @p_decorate
    def get_fullname(self):
        return self.first_name + " " + self.last_name

me = Person()
print(me.get_fullname())



# Passing params to decorators
def tags(tag_name):
    def tags_decorator(func):
        def func_wrapper(*args, **kwargs):
            return "<{0}>{1}</{0}>".format(tag_name, func(*args, **kwargs))
        return func_wrapper
    return tags_decorator

@tags("div")
@tags("p")
@tags("strong")
def get_text(name):
    return "My name is {0}, and I like Python!".format(name)

print(get_text("CB"))

# Outputs <div><p><strong>My name is CB, and I like Python!</strong></p></div>
