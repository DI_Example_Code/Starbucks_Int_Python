# def outer():
# 	msg = "Outside!"

# 	def inner():
# 		msg = "Inside!"
# 		print(msg)

# 	inner()
# 	print(msg)

# outer()



def outer():
	msg = "Outside!"

	def inner():
		nonlocal msg
		msg = "Inside!"
		print(msg)

	inner()
	print(msg)

outer()