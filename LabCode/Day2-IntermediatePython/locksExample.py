

import threading

lock = threading.Lock()

# pass False to acquire to prevent it from blocking
# if the lock is held by someone else
if not lock.acquire(False):
	# failed to lock the resource
else:
	try:
		# access the shared resource
	finally:
		lock.release()


		