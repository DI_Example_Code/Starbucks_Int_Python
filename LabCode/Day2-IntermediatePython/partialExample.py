def power(base, exponent):
	return base ** exponent

def square(base):
	return power(base, 2)

def cube(base):
	return power(base, 3)

print("2 squared =", square(2))
print("3 cubed =", cube(3))


# WITH PARTIAL

from functools import partial

def power(base, exponent):
	return base ** exponent

square = partial(power, exponent = 2)
cube = partial(power, exponent = 3)

print("2 squared =", square(2))
print("3 cubed =", cube(3))


# OR

# class Vehicle(object):
# 	def __init__(self, type, wheelCount):
# 		self.numWheels = wheelCount
# 		self.name = type

# 	def displayString(self):
# 		print(self.__class__.__name__, "(", self.name, ") with", self.numWheels, "wheels")

# v1 = Vehicle("car", 4)

# trike = partial(Vehicle, wheelCount = 3)("trike")

# v1.displayString()
# trike.displayString()

