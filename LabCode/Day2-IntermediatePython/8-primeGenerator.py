import math

def isPrime(number):
    if number > 1:
        if number == 2:
            return True

        if number % 2 == 0:
            return False

        for current in range(3, int(math.sqrt(number) + 1), 2):
            if number % current == 0: 
                return False

        return True

    return False


def primesGenerator():
	theList = range(2,100000)
	for x in theList:
		if (isPrime(x)):
			yield(x)

theGenerator = primesGenerator()

print("Sum of primes from 2 to 100,000 = ", sum(theGenerator))