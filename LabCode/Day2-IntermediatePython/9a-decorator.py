
# Define a function within another function
def greet(name):
    def get_message():
        return "Hello "

    result = get_message() + name + "!"
    return result

greet_someone = greet
print(greet_someone("CB"))

# Outputs: Hello CB!


# Return a function from a function
def generic_greeting():
	def get_message():
		return "Hello there!"

	return get_message

myGreeting = generic_greeting()

print(myGreeting())

# Outputs: Hello there!


# Putting it all together
def celebrityName(firstName):
	nameIntro = "This celebrity is "

	# this inner function has access to the outer function's variables,
	# including the parameter​
	def lastName(theLastName):
		return nameIntro + firstName + " " + theLastName

	return lastName


celebNameFunc = celebrityName("Johnny")
# At this point, the celebrityName outer function has returned.​

# The closure (lastName) is called here after the outer function has
# returned above​, but the closure still has access to the outer
# function's variables and parameter​
print(celebNameFunc("Depp")) # Prints 'This celebrity is Johnny Depp'


# Building a HTML paragraph wrapper

def get_text(name):
   return "My name is {0}, and I like Python!".format(name)

def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))
   return func_wrapper

my_get_text = p_decorate(get_text)

print(my_get_text("Christopher"))

# Outputs: <p>My name is Christopher, and I like Python!</p>


# Use the Python syntactic-sugar

def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))

   return func_wrapper

@p_decorate
def get_text(name):
   return "My name is {0}, and I like Python!".format(name)

print(get_text("Christopher"))

# Outputs <p>My name is Christopher, and I like Python!</p>


# Lab code
def p_decorate(func):
   def func_wrapper(name):
       return "<p>{0}</p>".format(func(name))

   return func_wrapper

def strong_decorate(func):
	def func_wrapper(name):
		return "<strong>{0}</strong>".format(func(name))

	return func_wrapper

def div_decorate(func):
	def func_wrapper(name):
		return "<div>{0}</div>".format(func(name))

	return func_wrapper

@div_decorate
@p_decorate
@strong_decorate
def get_text(name):
	return "My name is {0}, and I like Python!".format(name)

print(get_text("Christopher"))

