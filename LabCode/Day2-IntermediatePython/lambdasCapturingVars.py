x = 10
lam1 = lambda y: x + y
x = 20
lam2 = lambda y: x + y
print(lam1(10))
print(lam2(10))


# x = 10
# lam1 = lambda y, x = x: x + y
# x = 20
# lam2 = lambda y, x = x: x + y
# print(lam1(10))
# print(lam2(10))