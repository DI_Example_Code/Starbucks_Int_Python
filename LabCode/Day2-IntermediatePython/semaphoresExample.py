

import threading

maxConnections = 5
sem = BoundedSemaphore(value = maxConnections)

sem.acquire()
# connect to the DB
# execute query
# close DB connection
sem.release()

