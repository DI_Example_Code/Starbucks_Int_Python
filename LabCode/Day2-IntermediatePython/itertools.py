import itertools as it
import operator as op

# ::: COUNT

# for i in it.count(10, 0.25):
# 	if i > 100:
# 		break

# 	print(i)


# ::: CYCLE

# ctr = 0
# for i in it.cycle(['A', 'B', 'C', 'D']):
# 	print(i)
# 	ctr += 1

# 	if ctr > 100:
# 		break


# ::: REPEAT

# for i in it.repeat("FOO!", 12):
# 	print(i)

# print("Done!")

# --- OR

# rpt = it.repeat(7, 7)
# print(next(rpt))


# ::: ACCUMULATE

# sumList = list(it.accumulate(range(10)))

# multList = list(it.accumulate(range(1,10), op.mul))

# print(sumList)
# print(multList)


# ::: CHAIN

# initialsList = ["CB", "TC", "TCCB", "ABCB"]
# numList = list(range(8,12))
# listChain = list(it.chain(initialsList, numList))
# print(listChain)


# ::: COMPRESS

# letterString = "ABCDEFGH"
# selectors = [0,1,1,0,1,0,0,1]
# compList = list(it.compress(letterString, selectors))
# print(compList)


# ::: PRODUCT

# tupleList = [(1,2), (3,4), (5,6), (7,8)]
# cartProd = list(it.product((*tupleList)))
# print(cartProd)


# ::: COMBINATIONS

# combos = list(it.combinations("XYZ", 2))
# print(combos)


# ::: PERMUTATIONS

# perms = list(it.permutations("XYZ", 2))
# print(perms)







