class Vehicle(object):
	def __init__(self, wheelCount, seatCount, mileageVal):
		self.numWheels = wheelCount
		self.numSeats = seatCount
		self.mileage = mileageVal

	def honk(self):
		raise NotImplementedError

	def drive(self, delta):
		self.mileage += delta



class Bike(Vehicle):
	def __init__(self, aColor):
		super(Bike, self).__init__(2, 1, 0)
		self.color = aColor

	def honk(self):
		print(self.color, "Bike says Ah-Oooga!")

	def drive(self, delta = 10):
		print("Driving Bike...")

		super(Bike, self).drive(delta)
		
		print("Mileage is now", self.mileage)

		return self



class Car(Vehicle):
	def __init__(self, aModel):
		super(Car, self).__init__(4, 4, 20000)
		self.model = aModel

	def honk(self):
		print(self.model, "says Beep-Beep!")

	def drive(self, delta = 50):
		print("Driving Car...")

		super(Car, self).drive(delta)

		print("Mileage is now", self.mileage)

		return self


b = Bike("Blue")
c = Car("Toyota")

b.drive().drive().drive()

print("Driving Bike now...")
b.drive(50)

for i in range(3):
	c.drive()

c.drive(207)

b.honk()
c.honk()










