class FileCM(object):
	def __init__(self, file_name, method):
		self.file_obj = open(file_name, method)

	def __enter__(self):
		return self.file_obj

	def __exit__(self, type, value, traceback):
		if type:
			print("Exception handled in EXIT method")

		self.file_obj.close()
		return True


#	Use the context manager to write a file
with FileCM('demo.txt', 'w') as opened_file:
	opened_file.write('Context Managers ROCK!')

#	Now use the context manager to handle an error
with FileCM('demo.txt', 'w') as opened_file:
	opened_file.write_using_this_nonexistant_method('Context Managers ROCK!')



