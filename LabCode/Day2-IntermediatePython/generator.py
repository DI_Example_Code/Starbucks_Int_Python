squaresGenerator = (x * x for x in range(10))

for square in squaresGenerator:
	print(square)


# OR
print("\n-- OR --\n")

def squaresGenerator():
	theList = range(10)
	for x in theList:
		yield(x * x)

theGenerator = squaresGenerator()

for i in theGenerator:
	print(i)