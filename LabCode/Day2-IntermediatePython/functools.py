
from functools import partial

def log (message, subsystem):
    # Write the contents of 'message' to the specified subsystem.
    print("{0}: {1}".format(subsystem, message))

server_log = functools.partial(log, subsystem="server")
server_log("Unable to open socket")


