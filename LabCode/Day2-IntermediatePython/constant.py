# This is a work-around for constants in Python
# The idea being we create a class to represent the constant-space
# and then our values are attributes.
# Overriding the __setattr__ method prevents changing the values.

class CONST():
	FOO = 123
	BAR = 987
	TWERP = 'Hi there!'

	def __setattr__(self, *_):
		pass


CONST = CONST()

print(CONST.FOO)
print(CONST.BAR)
print(CONST.TWERP)

CONST.TWERP = "Goodbye!"
print(CONST.TWERP)