# 1
for num in range(1,1000):
	if num % 2: print(num)


# 2
# for num in range(7,1000,7):
# 	print(num)


# # 3
# accum = 0
# for num in range(10):
# 	accum += num**2

# print(accum)


# # 4
# accum = ctr = 0
# for num in range(5,1000,10):
# 	ctr += 1
# 	accum += num

# print(accum/ctr)

# # or
# print(sum(num for num in range(5,1000,10)) / len(range(5,1000,10)))

# # or
# from statistics import mean
# print(mean(num for num in range(5,1000,10)))