# 1
# def oddEven(start = 1, stop = 200):
# 	for x in range(start, stop):
# 		print("The number is {0}. It is an {1} number.".format(x, ("odd" if x % 2 else "even")))

# oddEven(3, 9)


# 2
def multiply(anArr, aNum):
	return list(map(lambda x: x * aNum, anArr))

print(multiply([2,3,4,5], 7))


# 3
def drawStars(anArr):
    for num in anArr:
        starStr = ""
        for i in range(0, num):
            starStr += "*"

        print(starStr)

drawStars([7,2,13,28])

### OR ###
# def drawStars2(anArr):
#     for count in range(0, len(anArr)):
#         print('*' * anArr[count])

# drawStars2([7,2,13,28])


# 4
# def modifiedDrawStars(anArr):
#     for count in range(0, len(anArr)):
#         val = anArr[count]

#         if isinstance(val, int):
#             print('*' * val)

#         else:
#             firstLetter = val[0].lower()

#             print(firstLetter * len(val))


# modifiedDrawStars([7, "Christopher", 3, "Xylophone", 1, "Zebra"])
