# 1
x = 5

# 2
# print(x)

# # 3
# x = "Chris"

# # 4
# print(x)

# # 5
# y = "topher"

# # 6
# z = x + y
# print(z)

# # 7
# i = 11
# print(z + str(i))

# # 8
# print(z.lower())
# print(z.upper())
# print(z.capitalize())
# print(z.swapcase())

# # 9
# str = "This is a five-word string"
# print(str.split())

# # 10
# str = "I am a dog, and a dog meows!"
# print(str.replace("dog", "cat"))