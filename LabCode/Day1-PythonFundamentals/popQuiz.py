\#1	How does Python delimit blocks of code?
#		indentation (NOT curly braces) and a color

#2	Turn 'Cool Cat' into 'cOOL cAT'
'Cool Cat'.swapcase()

#3	Concatenate "Cat" and "Dog" and print the result
print 'Cat' + 'Dog'

#4	Is Python a typed language?
#		Yes, strongly and dynamically

#5	Create a dictionary with 3 key/value pairs, then iterate over it, printing each key and value
d = {'a1':'Cat', 'a2':'Dog', 'a3':'Ferret'}
for key in d:
	print("{} = {}".format(key, d[key]))
#	or
for k, v in d.items():
	print("{} = {}".format(k, v))


#6	Delete one of the elements of the dictionary
del d['a2']
#	or
d.pop('a2', None)

#7	Create a list with 3 color names, and add a new name so that it is the 3rd item
l = ['green', 'blue', 'purple']
l.insert(2, 'red')

#8	Check your list for 'red' and print "Found Red!" if it exists in the list, and "No Red" if not
print('Found Red!') if 'red' in l else print('No Red')

#9	Create a tuple with the numbers 1 through 10 and print the middle 4 values
t = (1,2,3,4,5,6,7,8,9,10)
print(t[3:7])

#10	What are some of the advantages of using a tuple over a list?
#		tuples typically contain heterogeneous (disparate) data, lists usually homogeneous (similar) data
#		tuples are immutable
#		tuples can be used as keys in a dictionary without hashing problems
#		iteration is slightly faster

