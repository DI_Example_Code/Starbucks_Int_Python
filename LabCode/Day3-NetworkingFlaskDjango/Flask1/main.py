from flask import Flask, render_template

app = Flask(__name__)

aDict = {"a": 1, "b": 2}

@app.route('/')
def index():
	return render_template("index.html", myObj=aDict, firstName="Christopher", lastName="Burns")


app.run(debug=True)