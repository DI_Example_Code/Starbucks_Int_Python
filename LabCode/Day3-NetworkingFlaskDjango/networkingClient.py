import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	# connect, passing a tuple of (URL, port)
	s.connect(('www.google.com', 80))

	# send request
	s.send("GET /index.html HTTP/1.0\n\n".encode())

	# get 10000 bytes of the response
	data = s.recv(10000).decode()
	
	print(data)
