
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	# bind to port 9393
	s.bind(("",9393))

	# listen to, at most, 5 connection requests
	s.listen(5)

	# begin infinite loop
	while True:
	    clientSocket, address = s.accept()
	    print("Received connection from", address)
	    clientSocket.send(("Hello " + address[0] + "\n").encode())
	    clientSocket.close()


    