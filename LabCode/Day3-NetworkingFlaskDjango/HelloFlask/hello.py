from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def helloWorld():
	return "Hello from Flask!"
    # return render_template('index.html', name="Chris")

@app.route('/success')
def success():
    return render_template('success.html')

app.run(debug=True)