from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseNotFound
from .models import Question, Choice

def index(request):
    questions = Question.objects.all()
    
    questionData = {
        "questions": questions
    }
    
    return render(request, 'quiz/index.html', questionData)

def show(request, questionNumber):
    if int(questionNumber) < 1:
        return HttpResponseNotFound('<h1>Page not found...</h1>')
    
    q = Question.objects.get(id=questionNumber)
    choices = Choice.objects.all().filter(question=q)
    
    data = {
        "question": q,
        "choices": choices
    }
    
    return render(request, 'quiz/show.html', data) 
 
