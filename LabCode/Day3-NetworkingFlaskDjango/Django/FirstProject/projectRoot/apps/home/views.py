from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseNotFound

def index(request):
    pageData = {
        'name': 'Christopher Burns',
        'activity': 'swordfight',
        'progLang': 'iOS',
    }
    
    return render(request, 'home/index.html', pageData)