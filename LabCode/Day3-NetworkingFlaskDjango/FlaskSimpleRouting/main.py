from flask import Flask, render_template
import sqlite3

sqlite_file = 'my_db.db'

app = Flask(__name__)

aDict = {"a": 1, "b": 2, "c": 3}

@app.route('/')
def index():
	return render_template("index.html", myObj=aDict, firstName="Christopher", lastName="Burns")


@app.route('/users')
def displayUsersPage():
	userList = None

	with sqlite3.connect(sqlite_file) as conn:
		c = conn.cursor()
		c.execute('SELECT * FROM user')
		userList = c.fetchall()

		print(userList)

	return render_template("users.html", userList=userList)


@app.route('/help')
def displayHelpPage():
	catList = None

	with sqlite3.connect(sqlite_file) as conn:
		c = conn.cursor()
		c.execute('SELECT * FROM help_category')
		catList = c.fetchall()

	return render_template("help.html", catList=catList)



app.run(debug=True)