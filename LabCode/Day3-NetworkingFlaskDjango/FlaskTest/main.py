

import sqlite3

sqlite_file = 'my_db.db'

with sqlite3.connect(sqlite_file) as conn:
	c = conn.cursor()

	c.execute('SELECT * FROM user')
	print(c.fetchall())

	c.execute('SELECT * FROM help_category')
	print(c.fetchall())

