from flask import Flask, render_template, request, redirect
import json
import re
import pymysql.cursors

# connect to the DB
db = pymysql.connect(	host='localhost',
						port=8889,
						user='root',
						password='root',
						db='redhorse',
						charset='utf8mb4',
						cursorclass=pymysql.cursors.DictCursor)

# get a cursor
cursor = db.cursor()

# create out Flask WSGI app
app = Flask(__name__)

# make a few validation expressions
NO_NUMS_REGEX = re.compile(r'^[a-zA-Z]+$')
PW_REGEX = re.compile(r'\d.*[A-Z]|[A-Z].*\d')
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9\.\+_-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]*$')


# ROUTES
@app.route('/')
def index():
	sql = "SELECT * FROM user"
	cursor.execute(sql)
	res = cursor.fetchall()

	return json.dumps(res)


@app.route('/user', methods=['GET'])
def showNewUser():
	respStr = "<form action='/user' method='POST'>"
	respStr += "First Name: <input type='text' name='firstName' /><br />"
	respStr += "Last Name: <input type='text' name='lastName' /><br />"
	respStr += "Email: <input type='text' name='email' /><br />"
	respStr += "Password: <input type='password' name='password' /><br />"
	respStr += "Confirm Password: <input type='password' name='confirmPassword' /><br />"
	respStr += "<input type='submit' value='SUBMIT' /></form>"

	return respStr


@app.route('/user', methods=['POST'])
def addUser():
	if len(request.form['firstName']) < 1 or not NO_NUMS_REGEX.match(request.form['firstName']):
		return '{errorCode: 1, msg: Invalid First Name}'

	if len(request.form['lastName']) < 1 or not NO_NUMS_REGEX.match(request.form['lastName']):
		return '{errorCode: 2, msg: Invalid Last Name}'
	
	if len(request.form['email']) < 1 or not EMAIL_REGEX.match(request.form['email']):
		return '{errorCode: 3, msg: Email Invalid}'

	if len(request.form['password']) < 1:
		return '{errorCode:10, msg: Password Required}'
	elif len(request.form['password']) < 8:
		return '{errorCode:11, msg: Password must be at least 8 characters}'
	elif not PW_REGEX.match(request.form['password']):
		return '{errorCode:12, msg: Password must have at least one NUMBER and at least one UPPERCASE LETTER}'
	elif not (request.form['password'] == request.form['confirmPassword']):
		return '{errorCode:13, msg: Passwords do not match}'

	sql = "INSERT INTO user (first_name, last_name, email, password) VALUES (%s, %s, %s, %s)"
	cursor.execute(sql, (request.form['firstName'], request.form['lastName'], request.form['email'], request.form['password']))
	db.commit()

	return redirect('/')


@app.route('/user/<userID>', methods=['PUT'])
def updateUser(userID):
	sql = "UPDATE user SET first_name = %s, last_name = %s, email = %s, password = %s WHERE id = %s"
	cursor.execute(sql, (request.form['firstName'], request.form['lastName'], request.form['email'], request.form['password'], userID))
	db.commit()

	return json.dumps({'result': 'success'})


@app.route('/user/<userID>', methods=['DELETE'])
def deleteUser(userID):
	sql = "DELETE FROM user WHERE id = %s"
	cursor.execute(sql, (userID))
	db.commit()

	return json.dumps({'result': 'success'})


app.run(debug=True)