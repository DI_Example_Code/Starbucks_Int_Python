from flask import Flask, render_template, request, redirect
import json
import re
import pymysql.cursors

# connect to the DB
db = pymysql.connect(	host='localhost',
						port=8889, user='root',
						password='root',
						db='mysql',
						charset='utf8mb4',
						cursorclass=pymysql.cursors.DictCursor)

# get a cursor
cursor = db.cursor()

# create out Flask WSGI app
app = Flask(__name__)

# make a few validation expressions
NO_NUMS_REGEX = re.compile(r'^[a-zA-Z]+$')
PW_REGEX = re.compile(r'\d.*[A-Z]|[A-Z].*\d')
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9\.\+_-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]*$')


# ROUTES
@app.route('/')


@app.route('user', methods=['GET'])


@app.route('user', methods=['POST'])


@app.route('user/<userID>', methods=['PUT'])


@app.route('user/<userID>', methods=['DELETE'])